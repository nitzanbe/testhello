// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyB-TLu7atkjDi83naXRyogI4dTzE3NVGrg",
    authDomain: "hello-jce-test.firebaseapp.com",
    databaseURL: "https://hello-jce-test.firebaseio.com",
    projectId: "hello-jce-test",
    storageBucket: "hello-jce-test.appspot.com",
    messagingSenderId: "597776571924",
    appId: "1:597776571924:web:3a3e1487be9fbc86f91101"
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
