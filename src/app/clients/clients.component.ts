import { StudentsService } from './../students.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../auth.service';
import { PredictionService } from '../prediction.service';
import { Student } from '../interfaces/student';

@Component({
  selector: 'clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  ifpay: string;
  pays: string[] = ['Yes', 'No'];
  userId:string;
  @Input() math:string;
  @Input() psyc:string;
  @Output() update = new EventEmitter<Student>();


  add(math,psyc){
    this.studentService.addStudent(this.userId,math,psyc); 
  }


  constructor(public authService:AuthService,private predictionService:PredictionService, private studentService:StudentsService) { }

  ngOnInit(): void {
  }

}
