import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { StudentsService } from '../students.service';

@Component({
  selector: 'students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  
  email:string;
  users$;
  students$;
  panelOpenState = false;
  userId:string;

  constructor(public authService:AuthService, private studentsService:StudentsService) { }

  public deleteStudent(id:string){
    this.studentsService.deleteStudent(this.userId,id);
  }

  ngOnInit(): void {
    this.users$ = this.authService.getUser();
    this.users$.subscribe(
      data => {
        this.email = data.email;
      }
    )
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
        this.students$ = this.studentsService.getStudents(this.userId);
      }
    )
  }


}
