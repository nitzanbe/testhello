import { ClientsComponent } from './clients/clients.component';
import { Student } from './interfaces/student';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  studentCollection:AngularFirestoreCollection; 
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 
  @Output() update = new EventEmitter<Student>(); 

  addStudent(userId:string,math:string,psyc:string){
    const student = {math:math, psyc:psyc}; 
    this.userCollection.doc(userId).collection('students').add(student);
  }


  public getStudents(userId){
    this.studentCollection = this.db.collection(`users/${userId}/students`);
    return this.studentCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
        }
      )
    ))
  }

  public deleteStudent(userId:string , id:string){
    this.db.doc(`users/${userId}/students/${id}`).delete();
  }

  constructor(private db:AngularFirestore) { }
}
