export interface Student {
    id:string,
    math: string,
    psyc: string,
    pay?:boolean        
    predict? :string,
    ST? :number
}
