import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PredictionService {

  url =  'https://crlowxbck8.execute-api.us-east-1.amazonaws.com/test1';

  predict(math: number, psyc: number, pay: boolean):Observable<any>{
    let json = {
      "data": 
        {
          "math": math,
          "psyc": psyc,
          "pay": pay
        }
    }
    let body  = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        return res.body;       
      })
    );      
  }

  constructor(private http: HttpClient) { }
}
